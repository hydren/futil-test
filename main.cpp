/*
 * main.cpp
 *
 *  Created on: 10 de jul de 2017
 *      Author: hydren
 */

#include <iostream>
using std::cout; using std::endl;

#include <string>
using std::string;

#include "futil/language.hpp"
#include "futil/strtox.h"
#include "futil/stox.hpp"
#include "futil/string_parse.hpp"
#include "futil/properties.hpp"

void test(bool passed)
{
	cout << (passed? "passed" : "not passed") << endl;
}

void run_strtox_tests()
{
	cout << "starting strtoX tests" << endl;

	cout << "testing strtoi...";
	const char* intExampleTxt = "128";
	const int intExample = 128;
	test(futil::strtoi(intExampleTxt, null, 0) == intExample);

	cout << "testing strtol...";
	const char* longExampleTxt = "128";
	const long longExample = 128;
	test(futil::strtol(longExampleTxt, null, 0) == longExample);

	cout << "testing strtoh...";
	const char* shortExampleTxt = "128";
	const short shortExample = 128;
	test(futil::strtoh(shortExampleTxt, null, 0) == shortExample);

	cout << "testing strtou...";
	const char* unsignedExampleTxt = "128";
	const unsigned unsignedExample = 128;
	test(futil::strtou(unsignedExampleTxt, null, 0) == unsignedExample);

	cout << "testing strtoul...";
	const char* unsignedLongExampleTxt = "128";
	const unsigned long unsignedLongExample = 128;
	test(futil::strtoul(unsignedLongExampleTxt, null, 0) == unsignedLongExample);

	cout << "testing strtouh...";
	const char* unsignedShortExampleTxt = "128";
	const unsigned short unsignedShortExample = 128;
	test(futil::strtouh(unsignedShortExampleTxt, null, 0) == unsignedShortExample);

	cout << "testing strtoc...";
	const char* signedCharExampleTxt = "64";
	const signed char signedCharExample = 64;
	test(futil::strtoc(signedCharExampleTxt, null, 0) == signedCharExample);

	cout << "testing strtouc...";
	const char* unsignedCharExampleTxt = "64";
	const unsigned short unsignedCharExample = 64;
	test(futil::strtouc(unsignedCharExampleTxt, null, 0) == unsignedCharExample);

	cout << "testing strtof...";
	const char* floatExampleTxt = "123.456";
	const float floatExample = 123.456f;
	test(futil::strtof(floatExampleTxt, null) == floatExample);

	cout << "testing strtod...";
	const char* doubleExampleTxt = "123.456";
	const double doubleExample = 123.456;
	test(futil::strtod(doubleExampleTxt, null) == doubleExample);

	/*
	cout << "testing strtold...";
	const char* longDoubleExampleTxt = "123.456";
	const long double longDoubleExample = 123.456;
	test(futil::strtold(longDoubleExampleTxt, null) == longDoubleExample);
	*/
}

void run_stox_tests()
{
	cout << "starting stoX tests" << endl;

	cout << "testing stoi...";
	string intExampleTxt = "128";
	const int intExample = 128;
	test(futil::stoi(intExampleTxt, null, 0) == intExample);

	cout << "testing stol...";
	string longExampleTxt = "128";
	const long longExample = 128;
	test(futil::stol(longExampleTxt, null, 0) == longExample);

	cout << "testing stoh...";
	string shortExampleTxt = "128";
	const short shortExample = 128;
	test(futil::stoh(shortExampleTxt, null, 0) == shortExample);

	cout << "testing stou...";
	string unsignedExampleTxt = "128";
	const unsigned unsignedExample = 128;
	test(futil::stou(unsignedExampleTxt, null, 0) == unsignedExample);

	cout << "testing stoul...";
	string unsignedLongExampleTxt = "128";
	const unsigned long unsignedLongExample = 128;
	test(futil::stoul(unsignedLongExampleTxt, null, 0) == unsignedLongExample);

	cout << "testing stouh...";
	string unsignedShortExampleTxt = "128";
	const unsigned short unsignedShortExample = 128;
	test(futil::stouh(unsignedShortExampleTxt, null, 0) == unsignedShortExample);

	cout << "testing stoc...";
	string signedCharExampleTxt = "64";
	const signed char signedCharExample = 64;
	test(futil::stoc(signedCharExampleTxt, null, 0) == signedCharExample);

	cout << "testing stouc...";
	string unsignedCharExampleTxt = "64";
	const unsigned short unsignedCharExample = 64;
	test(futil::stouc(unsignedCharExampleTxt, null, 0) == unsignedCharExample);

	cout << "testing stof...";
	string floatExampleTxt = "123.456";
	const float floatExample = 123.456f;
	test(futil::stof(floatExampleTxt, null) == floatExample);

	cout << "testing stod...";
	string doubleExampleTxt = "123.456";
	const double doubleExample = 123.456;
	test(futil::stod(doubleExampleTxt, null) == doubleExample);
}

void run_parse_string_tests()
{
	cout << "starting parse tests" << endl;

	cout << "testing parse<int>...";
	string intExampleTxt = "128";
	const int intExample = 128;
	test(futil::parse<int>(intExampleTxt) == intExample);

	cout << "testing parse<long>...";
	string longExampleTxt = "128";
	const long longExample = 128;
	test(futil::parse<long>(longExampleTxt) == longExample);

	cout << "testing parse<short>...";
	string shortExampleTxt = "128";
	const short shortExample = 128;
	test(futil::parse<short>(shortExampleTxt) == shortExample);

	cout << "testing parse<unsigned>...";
	string unsignedExampleTxt = "128";
	const unsigned unsignedExample = 128;
	test(futil::parse<unsigned>(unsignedExampleTxt) == unsignedExample);

	cout << "testing parse<unsigned long>...";
	string unsignedLongExampleTxt = "128";
	const unsigned long unsignedLongExample = 128;
	test(futil::parse<unsigned long>(unsignedLongExampleTxt) == unsignedLongExample);

	cout << "testing parse<unsigned short>...";
	string unsignedShortExampleTxt = "128";
	const unsigned short unsignedShortExample = 128;
	test(futil::parse<unsigned short>(unsignedShortExampleTxt) == unsignedShortExample);

	cout << "testing parse<signed char>...";
	string signedCharExampleTxt = "64";
	const signed char signedCharExample = 64;
	test(futil::parse<signed char>(signedCharExampleTxt) == signedCharExample);

	cout << "testing parse<unsigned char>...";
	string unsignedCharExampleTxt = "64";
	const unsigned char unsignedCharExample = 64;
	test(futil::parse<unsigned char>(unsignedCharExampleTxt) == unsignedCharExample);

	cout << "testing parse<float>...";
	string floatExampleTxt = "123.456";
	const float floatExample = 123.456f;
	test(futil::parse<float>(floatExampleTxt) == floatExample);

	cout << "testing parse<double>...";
	string doubleExampleTxt = "123.456";
	const double doubleExample = 123.456;
	test(futil::parse<double>(doubleExampleTxt) == doubleExample);

	cout << "testing parse<bool>...";
	string boolExampleTxt = "true";
	const bool boolExample = true;
	test(futil::parse<bool>(boolExampleTxt) == boolExample);
}

void run_properties_test()
{
	cout << "starting properties class tests" << endl;
	futil::Properties properties;

	cout << "testing Properties::containsKey on an empty instance...";
	test(properties.containsKey("something") == false);

	cout << "testing Properties::get on an empty instance...";
	test(properties.get("something").empty());

	cout << "testing Properties::get with the same missing key...";
	test(properties.get("something").empty());

	cout << "testing Properties::put error-free execution with simple string key-value...";
	properties.put("color", "green");
	cout << "passed" << endl;

	cout << "testing Properties::containsKey with missing key...";
	test(properties.containsKey("missing") == false);

	cout << "testing Properties::containsKey with the simple key inserted...";
	test(properties.containsKey("color"));

	cout << "testing Properties::get with the simple key-value inserted...";
	test(properties.get("color") == "green");

	cout << "testing Properties::put/get with simple string key, space-containing value...";
	properties.put("name", "Super Krypton Potion");
	test(properties.get("name") == "Super Krypton Potion");

	cout << "testing Properties::put/get with simple string key, integer-parseable value...";
	properties.put("grade", "5");
	test(properties.get("grade") == "5");

	cout << "testing Properties::getInteger with previous key-value...";
	test(properties.getInteger("grade") == 5);

	cout << "testing Properties::put/get with simple string key, float-parseable value...";
	properties.put("mass", "18.3");
	test(properties.get("mass") == "18.3");

	cout << "testing Properties::getDouble with previous key-value...";
	test(properties.getDouble("mass") == 18.3);

	cout << "testing Properties::put/get with simple string key, string value with multiple parseable values inside...";
	properties.put("dimensions", "10, 50.2");
	test(properties.get("dimensions") == "10, 50.2");

	cout << "testing Properties::get with missing key on a non-empty instance...";
	test(properties.get("unknown").empty());

	cout << "testing Properties::load error-free execution...";
	futil::Properties loadedProperties;
	loadedProperties.load("test.properties");
	cout << "passed" << endl;

	cout << "will now evaluate Properties::load correctness by testing its resulting loaded values" << endl;

	cout << "testing trying to get a missing key...";
	test(loadedProperties.get("unknown").empty());

	cout << "testing getting a simple key-value...";
	test(loadedProperties.get("valvetrain") == "DOHC");

	cout << "testing getting a simple key with a space-containing value...";
	test(loadedProperties.get("model") == "Milano V8");

	cout << "testing getting a simple key with a integer-parsed value...";
	test(loadedProperties.getInteger("gears") == 5);

	cout << "testing getting a simple key with a integer-parsed value (bis)...";
	test(loadedProperties.getInteger("mass") == 1270);

	cout << "testing getting a simple key with a float-parsed value...";
	test(loadedProperties.getDouble("displacement") == 2.6);

	cout << "testing getting a simple key with a float-parsed value (bis)...";
	test(loadedProperties.getDouble("power") == 200.0);

	cout << "testing Properties::store error-free execution...";
	properties.store("temp.properties");
	cout << "passed" << endl;

	cout << "will now evaluate Properties::store correctness by loading the generated file and testing its resulting loaded values" << endl;
	futil::Properties loadedStoredProperties;
	loadedStoredProperties.load("temp.properties");
	cout << "loaded generated file" << endl;

	cout << "testing trying to get a missing key...";
	test(loadedStoredProperties.get("unknown").empty());

	cout << "testing getting a simple key-value...";
	test(loadedStoredProperties.get("color") == "green");

	cout << "testing getting a simple key with a space-containing value...";
	test(loadedStoredProperties.get("name") == "Super Krypton Potion");

	cout << "testing getting a simple key with a integer-parseable value...";
	test(loadedStoredProperties.get("grade") == "5");

	cout << "testing getting the same key with its value parsed to integer type...";
	test(loadedStoredProperties.getInteger("grade") == 5);

	cout << "testing getting a simple key with a float-parseable value...";
	test(loadedStoredProperties.get("mass") == "18.3");

	cout << "testing getting the same key with its value parsed to double type...";
	test(loadedStoredProperties.getDouble("mass") == 18.3);

	cout << "testing Properties::storeUpdate error-free execution...";
	loadedProperties.put("gears", "6");
	loadedProperties.put("power", "195.5");
	loadedProperties.put("mass", "1350");
	loadedProperties.storeUpdate("test.properties");
	cout << "passed" << endl;

	cout << "will now evaluate Properties::storeUpdate correctness by loading the generated file and testing its resulting loaded values" << endl;
	futil::Properties loadedStoredUpdatedProperties;
	loadedStoredUpdatedProperties.load("test.properties");
	cout << "loaded stored/updated test file" << endl;

	cout << "testing trying to get a missing key...";
	test(loadedStoredUpdatedProperties.get("unknown").empty());

	cout << "testing getting unchanged simple key-value with colon separator...";
	test(loadedStoredUpdatedProperties.get("valvetrain") == "DOHC");

	cout << "testing getting unchanged simple key with a space-containing value...";
	test(loadedStoredUpdatedProperties.get("model") == "Milano V8");

	cout << "testing getting changed simple key with a integer-parsed value...";
	test(loadedStoredUpdatedProperties.getInteger("gears") == 6);

	cout << "testing getting changed simple key with a integer-parsed value (bis)...";
	test(loadedStoredUpdatedProperties.getInteger("mass") == 1350);

	cout << "testing getting changed simple key with a float-parsed value...";
	test(loadedStoredUpdatedProperties.getDouble("power") == 195.5);

	cout << "testing getting unchanged simple key with a float-parsed value with trailing space and colon separator...";
	test(loadedStoredUpdatedProperties.getDouble("displacement") == 2.6);

	cout << "will now evaluate Properties::load behavior by testing its resulting loaded values for special cases" << endl;
	futil::Properties loadedSpecialProperties;
	loadedSpecialProperties.load("test2.properties");

	cout << "testing getting a space-containing key...";
	test(loadedSpecialProperties.get("art mode") == "free");

	cout << "testing getting a colon-containing key...";
	test(loadedSpecialProperties.get("color:purple") == "A020F0");

	cout << "testing getting a space-containing key with a colon-delimited value...";
	test(loadedSpecialProperties.get("my choice") == "lasagna");

	cout << "testing if semicolon comment was treated as such...";
	test(loadedSpecialProperties.get(";semicolon").empty());

	cout << "starting sectioned properties class tests" << endl;

	futil::SectionedProperties cfg;

	cout << "testing SectionedProperties::containsSection on an empty instance...";
	test(cfg.containsSection("unknown") == false);

	cout << "testing SectionedProperties::containsKey on an empty instance...";
	test(cfg.containsKey("unknown", "something") == false);

	cout << "testing SectionedProperties::get on an empty instance...";
	test(cfg.get("unknown", "something").empty());

	cout << "testing SectionedProperties::get on an empty instance (bis)...";
	test(cfg.get("", "something").empty());

	cout << "testing SectionedProperties assignment error-free execution...";
	cfg["locale"]["language"] = "german";
	cout << "passed" << endl;

	cout << "testing SectionedProperties::containsSection with missing section...";
	test(cfg.containsSection("missing") == false);

	cout << "testing SectionedProperties::containsSection with simple section inserted...";
	test(cfg.containsSection("locale"));

	cout << "testing SectionedProperties::containsKey with missing section...";
	test(cfg.containsKey("missing", "anything") == false);

	cout << "testing SectionedProperties::containsKey with missing key...";
	test(cfg.containsKey("locale", "missing") == false);

	cout << "testing SectionedProperties::containsKey simple key and section inserted...";
	test(cfg.containsKey("locale", "language"));

	cout << "testing SectionedProperties::get with missing section...";
	test(cfg.get("missing", "whatever").empty());

	cout << "testing SectionedProperties::get with missing key...";
	test(cfg.get("locale", "missing").empty());

	cout << "testing SectionedProperties::get with simple section, key-value...";
	test(cfg.get("locale", "language") == "german");

	cout << "testing SectionedProperties [] operator access with simple section...";
	test(cfg["locale"].empty() == false);

	cout << "testing SectionedProperties [][] operator access with simple section, key-value...";
	test(cfg["locale"]["language"] == "german");

	cout << "testing SectionedProperties assignment with simple section, key-value...";
	cfg["locale"]["encoding"] = "utf8";
	test(cfg["locale"]["encoding"] == "utf8");

	cout << "testing SectionedProperties assignment with blank (\"global\") section, key-value...";
	cfg[""]["author"] = "frankstein";
	test(cfg[""]["author"] == "frankstein");
	
	cout << "testing SectionedProperties::load error-free execution...";
	futil::SectionedProperties loadedCfg;
	loadedCfg.load("test.ini");
	cout << "passed" << endl;

	cout << "will now evaluate SectionedProperties::load correctness by testing its resulting loaded values" << endl;

	cout << "testing getting simple key-value from simple section...";
	test(loadedCfg["general"]["splash"] == "yes");

	cout << "testing getting simple key-value from blank (\"global\") section...";
	test(loadedCfg[""]["encoding"] == "ansi");

	cout << "testing getting simple key-value from simple section (bis)...";
	test(loadedCfg["screen"]["fullscreen"] == "yes");

	cout << "testing getting integer-parseable value (and parsing it) from simple section and key...";
	test(loadedCfg["screen"]["width"] == "1920" and loadedCfg["screen"].getInteger("width") == 1920);

	cout << "testing getting integer-parseable value (and parsing it) from simple section and key (bis)...";
	test(loadedCfg["screen"]["height"] == "1080" and loadedCfg["screen"].getInteger("height") == 1080);

	cout << "testing getting float-parseable value (and parsing it) from simple section and key...";
	test(loadedCfg["screen"]["refresh"] == "59.9" and loadedCfg["screen"].getDouble("refresh") == 59.9);

	cout << "testing getting float-parseable value (and parsing it) from simple section and key (bis)...";
	test(loadedCfg["audio"]["volume"] == "0.75" and loadedCfg["audio"].getDouble("volume") == 0.75);

	cout << "testing getting space-containing value from simple section and key...";
	test(loadedCfg["audio"]["mode"] == "surround stereo");

	cout << "testing getting multiple-space-containing value from simple section and key...";
	test(loadedCfg["controls"]["joystick"] == "YCUBE720 USB Controller");

	cout << "testing getting simple key-value from space-containing section...";
	test(loadedCfg["user data"]["cloud"] == "no");

	cout << "testing getting slash-containing value from space-containing section and simple key...";
	test(loadedCfg["user data"]["path"] == "/local/share");

	cout << "testing getting space-containing value from space-containing section, simple key...";
	test(loadedCfg["user data"]["nickname"] == "J Dovey");

	cout << "testing SectionedProperties::store error-free execution...";
	cfg["boot"]["timeout"] = "20";
	cfg["boot"]["skip errors"] = "no";
	cfg["theme"]["scale"] = "1.4";
	cfg["theme"]["name"] = "clearlooks light";
	cfg.store("temp.ini", "Hello World");
	cout << "passed" << endl;

	cout << "will now evaluate SectionedProperties::store correctness by loading the generated file and testing its resulting loaded values" << endl;

	futil::SectionedProperties loadedStoredCfg;
	loadedStoredCfg.load("temp.ini");

	cout << "testing getting simple key-value from simple section...";
	test(loadedStoredCfg["locale"]["encoding"] == "utf8");

	cout << "testing getting simple key-value from blank (\"global\") section...";
	test(loadedStoredCfg[""]["author"] == "frankstein");

	cout << "testing getting simple key-value from simple section (bis)...";
	test(loadedStoredCfg["locale"]["language"] == "german");

	cout << "testing getting integer-parseable value (and parsing it) from simple section and key...";
	test(loadedStoredCfg["boot"]["timeout"] == "20" and loadedStoredCfg["boot"].getInteger("timeout") == 20);

	cout << "testing getting float-parseable value (and parsing it) from simple section and key...";
	test(loadedStoredCfg["theme"]["scale"] == "1.4" and loadedStoredCfg["theme"].getDouble("scale") == 1.4);

	cout << "testing getting space-containing value from simple section and key...";
	test(loadedStoredCfg["theme"]["name"] == "clearlooks light");

	cout << "testing getting simple value from simple section and space-containing key...";
	test(loadedStoredCfg["boot"]["skip errors"] == "no");

	cout << "will now evaluate SectionedProperties::load behavior by testing its resulting loaded values for special cases" << endl;
	futil::SectionedProperties loadedSpecialCfg;
	loadedSpecialCfg.load("test2.ini");

	cout << "testing getting space-containing value from global section and space-containing key...";
	test(loadedSpecialCfg[""]["preferred title"] == "good ol' dev");

	cout << "testing getting simple value from simple section and space-containing key...";
	test(loadedSpecialCfg["graphics"]["shadow detail"] == "high");

	cout << "testing getting integer-parseable value (and parsing it) from simple section and space-containing key...";
	test(loadedSpecialCfg["graphics"]["anistropic filtering"] == "8" and loadedSpecialCfg["graphics"].getInteger("anistropic filtering") == 8);

	cout << "testing getting float-parseable value (and parsing it) from simple section and space-containing key...";
	test(loadedSpecialCfg["graphics"]["resolution factor"] == "1.25" and loadedSpecialCfg["graphics"].getDouble("resolution factor") == 1.25);

	cout << "testing getting space-containing key-value from simple section...";
	test(loadedSpecialCfg["graphics"]["models detail"] == "very high");

	cout << "testing getting simple value from simple section and multiple-space-containing key...";
	test(loadedSpecialCfg["input"]["raw input data"] == "yes");
}

int main()
{
	cout << "test suite for futil" << endl;

	run_strtox_tests();
	run_stox_tests();
	run_parse_string_tests();
	run_properties_test();

	cout << "finished all tests..." << endl;
}
